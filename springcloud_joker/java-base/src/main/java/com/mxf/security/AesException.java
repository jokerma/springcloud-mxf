package com.mxf.security;

/**
 * AesException
 * AES加解密异常类
 * 
 * @author xiebin
 * @date 2023/06/14
 */
public class AesException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3596638635313443616L;
	/**
	 * 构造函数
	 * 
	 * @param message
	 * @param cause
	 */
    public AesException(String message, Throwable cause) {
        super(message, cause);
    }
}
