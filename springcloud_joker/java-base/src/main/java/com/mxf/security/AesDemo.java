package com.mxf.security;

import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;

/**
 * AesDemo
 *
 * @author maxiaofeng
 * @date 2024/4/30
 */
public class AesDemo {

    public static void main(String[] args) throws AesException {
        String str = "我是王家卫";
        String key = "ioguqxdmafzk9mqm";


        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, key.getBytes());
        System.out.println("key 的长度是: "+aes.getSecretKey().getEncoded().length);

//        byte[] encrypt = Aes128Pkcs7Utils.encrypt(str, key);
//        String result = new String(encrypt);
//        System.out.println(result);
        // Key length not 128/192/256 bits
        // https://blog.csdn.net/dctCheng/article/details/110187588


    }

}
