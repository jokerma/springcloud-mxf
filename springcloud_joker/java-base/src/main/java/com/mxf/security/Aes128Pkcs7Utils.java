package com.mxf.security;

import cn.hutool.core.codec.Base64;
import org.springframework.util.Assert;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;

/**
 * AES加解密工具类
 * 加解密模式：AES128/ECB/PKCS7Padding
 * Java默认不支持 PKCS7Padding 填充方式，使用bcprov-jdk15on提供的实现
 **/
public class Aes128Pkcs7Utils {
	/**
	 * 加密算法
	 */
	public static final String AES_ALGORITHM = "AES";
	/**
	 * 加解密模式
	 */
	public static final String AES_ECB_PKCS7 = AES_ALGORITHM +"/ECB/PKCS7Padding";

	static {
		// 添加Provider
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }
	/**
	 * 私有化构造函数
	 */
    private Aes128Pkcs7Utils() {

    }
    /**
     * AES加密
     *
     * @param src 待加密数据
     * @param key 密钥
     * @return 加密后的数据
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    public static byte[] encrypt(byte[] src,byte[] key) throws NoSuchAlgorithmException, 
    	NoSuchPaddingException,InvalidKeyException,IllegalBlockSizeException, BadPaddingException {
    	// 参数校验src
    	Assert.notNull(src,"null src");
		// 参数校验key
    	Assert.notNull(key,"null key");
    	// AES加密	
    	SecretKeySpec skeySpec = new SecretKeySpec(key, AES_ALGORITHM);
        Cipher cipher = Cipher.getInstance(AES_ECB_PKCS7);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        return cipher.doFinal(src); 	
    }
    /**
     * 加密，加密后的数据以Base64 字符串格式返回
     * 返回数据=Base64(AES(sSrc))
     * 
     * @param sSrc
     * @param sKey ASCII码组成的密码，不支持中文等其他密钥
     * @return 加密后Base64
     * @throws Exception
     */
    public static String encryptBase64(String sSrc, String sKey)  throws AesException {
    	byte[] encrypted = encrypt(sSrc,sKey);
        return Base64.encode(encrypted);  	
    }
    
    /**
     * 加密字符串原文
     * 
     * @param sSrc 明文数据 UTF-8编码格式
     * @param sKey 密钥 ASCII码组成的密码，不支持中文等其他密钥
     * @return
     * @throws Exception
     */
    public static byte[] encrypt(String sSrc, String sKey) throws AesException {
		// 参数校验sSrc
    	Assert.hasLength(sSrc,"empty sSrc:"+sSrc);
		// 参数校验sKey
    	Assert.hasLength(sKey,"empty sKey:"+sKey);
    	try {
    		return encrypt(sSrc.getBytes(StandardCharsets.UTF_8), 
        			sKey.getBytes(StandardCharsets.UTF_8));
		} catch (Exception e) {
			throw new AesException("Aes128Pkcs7Utils encrypt error:"+e.getMessage(),e);
		}  	
    }
    
    /**
     * AES解密
     * 
     * @param encrypted 密文数据
     * @param key       密钥
     * @return
     * @throws AesException
     */
    public static byte[] decrypt(byte[] encrypted,byte[] key) throws NoSuchAlgorithmException, 
		NoSuchPaddingException,InvalidKeyException,IllegalBlockSizeException, BadPaddingException{
    	// null参数校验
    	Assert.notNull(encrypted,"null encrypted text");
		// null key校验
    	Assert.notNull(key,"null key");   	
    	// AES解密 
		SecretKeySpec skeySpec = new SecretKeySpec(key, AES_ALGORITHM);
        Cipher cipher = Cipher.getInstance(AES_ECB_PKCS7);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        return cipher.doFinal(encrypted); 	    	
    }   
    /**
     * 解密Base64编码的密文，返回字符串
     * 明文加密前必须是UTF-8格式的字符串，否则调用该接口返回的可能是乱码
     * base64Str = Base64(AES(字符串明文))，解密执行相反过程
     * 
     * @param base64Str Base64编码的密文
     * @param key
     * @return
     * @throws AesException
     */
    public static String decryptBase64(String base64Str,String key)  throws AesException{
    	 byte[] encryptedBytes = Base64.decode(base64Str);
    	 try {
			byte[] original = decrypt(encryptedBytes, key.getBytes(StandardCharsets.UTF_8));
			return new String(original,StandardCharsets.UTF_8);
		} catch (Exception e) {
			throw new AesException("Aes128Pkcs7Utils decrypt error:"+e.getMessage(),e);
		}
    }
}
