package com.mxf.controller;

import com.mxf.entity.AddReq;
import com.mxf.entity.ProjectReqVO;
import com.mxf.entity.Result;
import com.mxf.service.ProjectInfoService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



/**
 * RequestOnlyController
 *
 * @author maxiaofeng
 * @date 2024/4/28
 */
@RestController
@RequestMapping("/anti-shake")
public class RequestOnlyController {
    @Autowired
    private ProjectInfoService projectInfoService;
    /**
     * 添加项目
     * @param reqVO
     * @return
     */
    @PostMapping(path = "/add")
    public Result<Integer> queryScanCodeSwitch(@RequestBody ProjectReqVO reqVO) {
        Integer num = projectInfoService.createProject(reqVO);
        return Result.success(num);
    }

}
