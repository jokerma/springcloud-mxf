package com.mxf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcloudSumProdApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudSumProdApplication.class, args);
    }

}
