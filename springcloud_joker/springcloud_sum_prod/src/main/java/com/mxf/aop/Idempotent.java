package com.mxf.aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * @Author: zcg
 * @Description: 幂等注解
 * @Date: 2024/3/12
 **/
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Idempotent {

    /**
     * 幂等的超时时间，默认为 1 秒
     *
     * 注意，如果执行时间超过它，请求还是会进来
     */
    int timeout() default 1;

    /**
     * 时间单位，默认为 SECONDS 秒
     */
    TimeUnit timeUnit() default TimeUnit.SECONDS;

    /**
     * redis锁前缀
     * @return
     */
    String keyPrefix() default "idempotent";

    /**
     * key分隔符
     * @return
     */
    String delimiter() default "|";

    /**
     * 提示信息，正在执行中的提示
     */
    String message() default "重复请求，请稍后重试";
}
