package com.mxf.service;

import com.mxf.entity.ProjectReqVO;

/**
 * ProjectInfoService
 *
 * @author maxiaofeng
 * @date 2024/4/29 10:10
 */
public interface ProjectInfoService {
    /**
     * 创建
     * @param reqVO
     * @return
     */
    Integer createProject(ProjectReqVO reqVO);
}
