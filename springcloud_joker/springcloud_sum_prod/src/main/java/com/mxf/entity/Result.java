package com.mxf.entity;

import lombok.Data;

/**
 * ResultData
 *
 * @author maxiaofeng
 * @date 2023/5/15 9:41
 */
@Data
public class Result<T> {
    /**
     * 返回编号
     */
    private String code;
    /**
     * 返回信息
     */
    private String message;
    /**
     * 返回数据
     */
    private T data;
    /**
     * 错误描述
     */
    private String desc;

    public static <T> Result<T> success(T num) {
        return new Result<T>("OK","",num,"");
    }

    public Result(String code, String message, T data, String desc) {
        this.code = code;
        this.message = message;
        this.data = data;
        this.desc = desc;
    }
}