package com.mxf.entity;

import lombok.Data;


/**
 * 项目管理 新增 VO
 * 
 */
@Data
public class ProjectReqVO {

    /**
     * 合同编号
     */
    private String contractNo;

    /**
     * 项目名字
     */
    private String name;

    /**
     * 项目状态
     */
    private Integer status;

}
