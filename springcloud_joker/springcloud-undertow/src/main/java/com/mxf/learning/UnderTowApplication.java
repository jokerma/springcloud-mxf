package com.mxf.learning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * UnderTowApplication
 *
 * @author maxiaofeng
 * @date 2024/4/26
 */
@SpringBootApplication
public class UnderTowApplication {
    public static void main(String[] args) {
        SpringApplication.run(UnderTowApplication.class,args);
    }
}
