package com.mxf.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ConfigController
 *
 * @author maxiaofeng
 * @date 2023/12/8
 */
@RestController
@RequestMapping("/config")
public class ConfigController {
    @Value("${config.info}")
    private String configInfo;
    @GetMapping("/get")
    public JSONObject getConfigInfo(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("success","0");
        jsonObject.put("data",configInfo);
        return jsonObject;
    }
}
