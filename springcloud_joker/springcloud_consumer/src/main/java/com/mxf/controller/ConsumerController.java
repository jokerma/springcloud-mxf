package com.mxf.controller;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/consumer")
@Slf4j
public class ConsumerController {
    @Value("${server.port}")
    private String serverPort;
    @Value("${spring.application.name}")
    private String serverName;
    @GetMapping("/serverPort")
    public JSONObject getServerPort(String param){
        log.info("======== get serverPort confilter {}",param);
        String result = serverName + "-" + serverPort;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("success","0");
        jsonObject.put("data",result);
        return jsonObject;
    }

    @GetMapping(value = "/custom")
    public String customTest(){
        return "网关配置测试~~costom";
    }
}
