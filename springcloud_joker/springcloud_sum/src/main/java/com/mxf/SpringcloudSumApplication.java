package com.mxf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * https://spring-cloud-alibaba-group.github.io/github-pages/hoxton/en-us/index.html#_introduction
 * nacos 启动类注解
 */
@SpringBootApplication
@EnableDiscoveryClient
public class SpringcloudSumApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudSumApplication.class, args);
    }

}
