package com.mxf.controller;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ConfigController
 *
 * @author maxiaofeng
 * @date 2023/12/8
 */
@RestController
@RequestMapping("/config")
@Slf4j
public class ConfigController {

    @Value("${user.name}")
    private String username;
    @Value("${user.age}")
    private Integer userage;
    @Value("${current.env}")
    private String currentEnv;

    @GetMapping("/getUserInfo")
    public JSONObject getUserInfo(){
        log.info("=== get User Info ===");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username",username);
        jsonObject.put("userage",userage);
        return jsonObject;
    }

    @GetMapping("/getEnv")
    public JSONObject getEnv(){
        log.info("=== get Env Info ===");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("success","1");
        jsonObject.put("env",currentEnv);
        return jsonObject;
    }
}
