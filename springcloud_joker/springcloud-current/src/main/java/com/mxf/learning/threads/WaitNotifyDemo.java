package com.mxf.learning.threads;

import java.util.ArrayList;
import java.util.List;

/**
 * WaitNotifyDemo
 *
 * @author maxiaofeng
 * @date 2024/4/29
 */
class TaskWN{

    private Integer capacity;
    List<String> queue;

    public TaskWN(Integer capacity, List<String> queue) {
        this.capacity = capacity;
        this.queue = new ArrayList<>(capacity);
    }

    public void add(String ele){
        synchronized (queue){
            while(queue.size()==capacity){
                try {
                    queue.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            queue.add(ele);
            queue.notifyAll();
        }
    }
}


public class WaitNotifyDemo {




}
