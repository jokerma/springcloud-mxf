package com.mxf.learning.completable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;

/**
 * MultiCompletableFutureDemo
 *
 * @author maxiaofeng
 * @date 2024/4/29
 */
public class MultiCompletableFutureDemo {
    // 创建异步任务
    public static CompletableFuture<String> doSomethingOne(String encodedCompanyId){
        return CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("------"+encodedCompanyId+"--------------");
                String id = encodedCompanyId;
                return id;
            }
        });
    }

    public static CompletableFuture<String> doSomethingRandom(String encodedCompanyId){
        return CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {
                int v = (int) (Math.random() * 1000);
                try {
                    Thread.sleep(v);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("------"+v+":"+encodedCompanyId+"--------------");
                String id = encodedCompanyId;
                return id;
            }
        });
    }

    public static CompletableFuture<String> doSomethingTwo(String companyId){
        return CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                String str = companyId+":alibaba";
                return str;
            }
        });
    }
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //comPose();
        //combine();
        //allOf();
        List<CompletableFuture<String>> futureList = new ArrayList<>();
        futureList.add(doSomethingRandom("1"));
        futureList.add(doSomethingRandom("2"));
        futureList.add(doSomethingRandom("3"));
        futureList.add(doSomethingRandom("4"));
        CompletableFuture<Object> future = CompletableFuture.anyOf(futureList.toArray(new CompletableFuture[futureList.size()]));
        System.out.println(future.get());

    }

    private static void allOf() throws InterruptedException, ExecutionException {
        List<CompletableFuture<String>> futureList = new ArrayList<>();
        futureList.add(doSomethingOne("1"));
        futureList.add(doSomethingOne("2"));
        futureList.add(doSomethingOne("3"));
        futureList.add(doSomethingOne("4"));
        CompletableFuture<Void> future = CompletableFuture.allOf(futureList.toArray(new CompletableFuture[futureList.size()]));
        System.out.println(future.get());
    }

    private static void combine() throws InterruptedException, ExecutionException {
        CompletableFuture<String> future = doSomethingOne("123").thenCombine(doSomethingTwo("456"), (one, two) -> {
            return one + " " + two;
        });
        System.out.println(future.get());
    }

    private static void comPose() throws InterruptedException, ExecutionException {
        CompletableFuture<String> result = doSomethingOne("123").thenCompose(id -> doSomethingTwo(id));
        System.out.println(result.get());
    }
}
