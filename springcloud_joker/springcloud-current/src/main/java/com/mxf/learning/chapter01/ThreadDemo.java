package com.mxf.learning.chapter01;

/**
 * ThreadDemo
 *
 * @author maxiaofeng
 * @date 2024/4/26
 */

public class ThreadDemo {
    public static void main(String[] args) {
        //extendThread();
        //extendRunnableImpl();
        //extendNiming();
        //extendLambda();
        for (int i = 0; i < 100; i++) {
            System.out.println("main: " + i);
        }
    }

    private static void extendLambda() {
        Thread thread = new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("lambda: " + i);
            }
        });
        thread.start();
    }

    private static void extendNiming() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 100; i++) {
                    System.out.println("匿名内部类:: " + i);
                }
            }
        });
        thread.start();
    }

    private static void extendRunnableImpl() {
        MyImpl my = new MyImpl();
        Thread thread = new Thread(my);
        thread.start();
    }

    private static void extendThread() {
        MyJob myJob = new MyJob();
        myJob.start();
    }

}

class MyJob extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("MyJob:" + i);
        }
    }
}

class MyImpl implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("MyImpl:" + i);
        }
    }
}
