package com.mxf.learning.threads;

/**
 * 锁重入机制实现方式
 *
 * *************************************************************************
 * <p/>
 *
 * @文件名称: ChildProgram.java
 * @包 路 径： cn.org.test
 * @版权所有：北京数字认证股份有限公司 (C) 2019
 * @类描述:
 * @版本: V1.0 @创建人：wangyangyang
 * @创建时间：2019-8-24 18:03
 */
public class ChildProgram extends SuperProgram {

    public static void main(String[] args) {
        ChildProgram childProgram = new ChildProgram();
        childProgram.childSomeSthing();
    }

    public synchronized void childSomeSthing (){
        superDoSomeSthing();
        System.out.println("child do Something");
    }

    @Override
    public synchronized void superDoSomeSthing() {
        System.out.println("child do Something");
        super.superDoSomeSthing();
    }
}

/**
 * *************************************************************************
 * <p/>
 *
 * @文件名称: SuperProgram.java
 * @包 路 径： cn.org.test
 */
class SuperProgram {

    public synchronized void superDoSomeSthing (){
        System.out.println("super,doing something");
    }
}

