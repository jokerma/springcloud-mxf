package com.mxf.learning.lambda;

import com.mxf.learning.entity.Person;

import java.util.ArrayList;
import java.util.List;

/**
 * LambdaStreamDemo
 *
 * @author maxiaofeng
 * @date 2024/4/29
 */
public class LambdaStreamDemo {

    public static List<Person> makeList(){
        List<Person> personList = new ArrayList<>();
        Person p1 = new Person();
        p1.setAge(10);
        p1.setName("zjc");
        personList.add(p1);
        Person p2 = new Person();
        p2.setAge(11);
        p2.setName("jzz");
        personList.add(p2);
        Person p3 = new Person();
        p3.setAge(5);
        personList.add(p3);
        p3.setName("lxd");
        return personList;
    }
    public static void noStream(List<Person> personList){
        List<String> nameList = new ArrayList<>();
        for (Person person : personList) {
            if(person.getAge()>10) {
                nameList.add(person.getName());
            }
        }
        for (String s : nameList) {
            System.out.println(s);
        }
    }

    public static void main(String[] args) {
        List<Person> personList = makeList();
        //noStream(personList);
        personList.stream().filter(vo->vo.getAge()>=10).map(Person::getName).toList().forEach(System.out::println);
    }
}
