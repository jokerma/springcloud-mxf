package com.mxf.learning.chapter01;

/**
 * ThreadState
 *
 * @author maxiaofeng
 * @date 2024/4/26
 */
public class ThreadState {
    private static volatile boolean flag=true;
    public static void main(String[] args) throws InterruptedException {

        Thread thread = new Thread();
        System.out.println(thread.getState());
        System.out.println("=================================");
        Thread threadRun = new Thread(() -> {
            System.out.println("执行开始");
            while (flag) {

            }
            System.out.println("执行结束");
        });
        threadRun.start();
        System.out.println(threadRun.getState());
        flag=false;
        System.out.println("=================================");
        Object lockObject = new Object();
        Thread threadBlock = new Thread(() -> {
            // t1线程拿不到锁资源，导致变为BLOCKED状态
            synchronized (lockObject) {

            }
            System.out.println("锁结束");
        });
        // main线程拿到obj的锁资源
        synchronized (lockObject){
            threadBlock.start();
            Thread.sleep(500);
            System.out.println(threadBlock.getState());
        }
        System.out.println("================================="+flag);
        Thread threadWaiting = new Thread(() -> {
            synchronized (lockObject) {
                try {
                    lockObject.wait();
                    // 执行....
                    if(flag){

                    }
                    System.out.println("被敲醒了");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        threadWaiting.start();
        Thread.sleep(500);
        System.out.println(threadWaiting.getState());
        synchronized (lockObject){
            lockObject.notifyAll();
        }
        System.out.println(threadWaiting.getState());
        Thread.sleep(500);
        System.out.println(threadWaiting.getState());



    }
}
