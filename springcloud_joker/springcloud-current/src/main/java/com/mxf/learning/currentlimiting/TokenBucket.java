package com.mxf.learning.currentlimiting;

import java.util.Date;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

// 声明一个令牌桶
public class TokenBucket {
    private final int capacity; // 仓库容量
    private final AtomicInteger tokens; // 当前剩余token数量
    private final int refillRate; // 每秒补充token数

    public TokenBucket(int capacity, int refillRate) {
        this.capacity = capacity;
        this.tokens = new AtomicInteger(capacity);
        this.refillRate = refillRate;

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        // 每隔 (1000 / refillRate) 毫秒补充一个令牌
        scheduler.scheduleAtFixedRate(() -> {
            if (tokens.get() < capacity) {
                tokens.incrementAndGet();
            }
        }, 0, 1000 / refillRate, TimeUnit.MILLISECONDS);
    }

    // 从令牌桶中消耗一个令牌
    public synchronized boolean tryConsume() {
        if (tokens.get() > 0) {
            tokens.decrementAndGet();
            return true;
        }
        return false;
    }

    public AtomicInteger getTokens() {
        return tokens;
    }

    /**
     * CountDownLatch
     * 用法说明:
     *
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        //testExecute();
        //increateIndex();


    }

    private static void increateIndex() throws InterruptedException {
        TokenBucket tokenBucket = new TokenBucket(200, 5);
        while (!(tokenBucket.getTokens().intValue()==(200))){
            Thread.sleep(1000/5+5);
            System.out.println(System.currentTimeMillis()+":"+tokenBucket.getTokens());
        }
    }

    private static void testExecute() {
        TokenBucket tokenBucket = new TokenBucket(5, 2);
        ExecutorService threadPool = Executors.newFixedThreadPool(10);
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

        // 每隔1s钟，再发起三个请求， 按照令牌桶的策略，之后的每秒，每次三个请求，都会被接收2个
        scheduler.scheduleAtFixedRate(() -> {
            CountDownLatch countDownLatch = new CountDownLatch(3);
            System.out.println("--------------------------start-------------------------------------->");
            for (int i = 0; i < 3; i++) {
                threadPool.execute(() -> {
                    System.out.println(new Date() + "Request " + Thread.currentThread().getId() + ": " + (tokenBucket.tryConsume() ? "Accepted" :
                            "Rejected"));
                    countDownLatch.countDown();
                });
            }
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {

            }
            System.out.println("--------------------------end-------------------------------------->");
        }, 0, 1000, TimeUnit.MILLISECONDS);
    }
}
