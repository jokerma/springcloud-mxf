package com.mxf.learning.chapter01;

/**
 * ThreadNotifyDemo
 *
 * @author maxiaofeng
 * @date 2024/4/26
 */
public class ThreadNotifyDemo {

    static volatile boolean flag = true;
    public static void main(String[] args) throws InterruptedException {
        //notifyAllss();
        //stopThread();
        // 使用共享变量停运
        // stopShareThread();
        System.out.println(Thread.currentThread().isInterrupted());
        Thread.currentThread().interrupt();
        System.out.println(Thread.currentThread().isInterrupted());
        System.out.println(Thread.interrupted());
        System.out.println(Thread.interrupted());
        //Thread.interrupted();
        System.out.println(Thread.currentThread().isInterrupted());
        System.out.println("=================================");
        Thread t1 = new Thread(() -> {
            System.out.println("开始任务");
            while (!Thread.currentThread().isInterrupted()) {
                //System.out.println("...");
            }
            System.out.println("任务结束");
        });
        t1.start();
        Thread.sleep(500);
        t1.interrupt();
    }

    private static void stopShareThread() throws InterruptedException {
        Thread thread = new Thread(() -> {
            while (flag) {

            }
            System.out.println("任务结束");
        });
        thread.start();
        Thread.sleep(500);
        flag=false;
    }

    private static void stopThread() throws InterruptedException {
        Thread t1 = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t1.start();
        Thread.sleep(500);
        t1.stop();
        System.out.println(t1.getState());
        Thread.sleep(10000);
        System.out.println(t1.getState());
        /**
         * TIMED_WAITING
         * TERMINATED
         */}

    private static void notifyAllss(){
        Thread t1 = new Thread(() -> {
            sync();
            System.out.println("t1:执行完");
        }, "t1");
        Thread t2 = new Thread(() -> {
            sync();
            System.out.println("t2:执行完");
        }, "t2");
        t1.start();
        t2.start();
        try {
            Thread.sleep(12000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        synchronized (ThreadNotifyDemo.class){
            ThreadNotifyDemo.class.notifyAll();
        }
        System.out.println("唤醒了所有");
    }


    public static synchronized void sync(){
        try {
            for (int i = 0; i < 100; i++) {
                if(i==5){
                    ThreadNotifyDemo.class.wait();
                }
                Thread.sleep(1000);
                System.out.println(Thread.currentThread().getName());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
