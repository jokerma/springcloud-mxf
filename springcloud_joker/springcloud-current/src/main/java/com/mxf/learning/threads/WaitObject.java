package com.mxf.learning.threads;

/**
 * WaitObject
 *
 * @author maxiaofeng
 * @date 2024/4/29
 */

class TaskJoker implements Runnable{

    private Object object;

    private boolean flag;

    public TaskJoker(Object object) {
        this.object = object;
    }

    @Override
    public void run() {
        synchronized (object){
            System.out.println(Thread.currentThread().getName()+":get lock");
            try {
                Thread.sleep(1000);
                object.wait();
                System.out.println(Thread.currentThread().getName()+"--------------");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(Thread.currentThread().getName()+"--- can't get lock ---");
    }
}

class TaskJokerInter implements Runnable{

    private Object object;

    private boolean flag;

    public TaskJokerInter(Object object) {
        this.object = object;
    }

    @Override
    public void run() {
        synchronized (object){
            System.out.println(Thread.currentThread().getName()+":release lock");
            try {
                object.notifyAll();
//                System.out.println("--------------");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
public class WaitObject {
    public static void main(String[] args) throws InterruptedException {
        Object object = new Object();
        TaskJoker taskJoker = new TaskJoker(object);
        TaskJoker taskEcho = new TaskJoker(object);
        TaskJokerInter taskNoti = new TaskJokerInter(object);
        Thread t1 = new Thread(taskJoker);
        System.out.println("1 state: "+t1.getState());
        t1.start();
        System.out.println("1 state: "+t1.getState());
        Thread t2 = new Thread(taskEcho);
        System.out.println("2 state: "+t2.getState());
        t2.start();
        System.out.println("2 state: "+t2.getState());
Thread.sleep(1000);
        System.out.println("1 state: "+t1.getState());
        System.out.println("2 state: "+t2.getState());

        Thread.sleep(2000);
        new Thread(taskNoti).start();


    }




}
