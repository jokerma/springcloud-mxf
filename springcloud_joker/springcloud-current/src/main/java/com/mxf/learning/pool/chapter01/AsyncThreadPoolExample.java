package com.mxf.learning.pool.chapter01;

import java.util.concurrent.*;

/**
 * AsyncThreadPoolExample
 *
 * @author maxiaofeng
 * @date 2024/4/28
 */
public class AsyncThreadPoolExample {
    public static String doSomethingA() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("--- doSomethingA ---");
        return "A Task Done";
    }

    private final static int AVALIABLE_PROCESSORS = Runtime.getRuntime().availableProcessors();
    private final static ThreadPoolExecutor POOL_EXECUTOR = new ThreadPoolExecutor(
            AVALIABLE_PROCESSORS,
            AVALIABLE_PROCESSORS * 2,
            1,
            TimeUnit.MINUTES,
            new LinkedBlockingDeque<>(5),
//            new ThreadFactory() {
//                @Override
//                public Thread newThread(Runnable r) {
//                    return null;
//                }
//            },
            new ThreadPoolExecutor.CallerRunsPolicy());

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Future<String> resultA = POOL_EXECUTOR.submit(() -> doSomethingA());
        System.out.println(resultA.get());
        SynchronousQueue<String> strings = new SynchronousQueue<>();
        PriorityBlockingQueue<Object> objects = new PriorityBlockingQueue<>();
        ArrayBlockingQueue arrayBlockingQueue = new ArrayBlockingQueue<>(10);
        LinkedBlockingDeque<Object> linkedBlockingDeque = new LinkedBlockingDeque<>();
        // 线程工厂

        // 饱和策略

        // 存活时间

    }
}
