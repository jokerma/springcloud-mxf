package com.mxf.learning.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Person
 *
 * @author maxiaofeng
 * @date 2024/4/29
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    private Integer age;
    private String name;

}
