package com.mxf.learning.completable;

import java.util.concurrent.*;

/**
 * FutureDemo
 *
 * @author maxiaofeng
 * @date 2024/4/28
 */
public class FutureTaskDemo {

    public static String doSomethingA(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("--- doSomethingA ---");
        return "TaskAResult";
    }

    public static String doSomethingB(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("--- doSomethingB ---");
        return "TaskBResult";
    }

    private final static int AVALIABLE_PROCESSORS = Runtime.getRuntime().availableProcessors();
    private final static ThreadPoolExecutor POOL_EXECUTOR = new ThreadPoolExecutor(
            AVALIABLE_PROCESSORS,
            AVALIABLE_PROCESSORS*2,
            1,
            TimeUnit.SECONDS,
            new LinkedBlockingDeque<>(5),
            new ThreadPoolExecutor.CallerRunsPolicy()
    );

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        long start = System.currentTimeMillis();
        //testDemo();
        FutureTask<String> futureTask = new FutureTask<>(() -> {
            String result = doSomethingA();
            return result;
        });
        POOL_EXECUTOR.execute(futureTask);
        Future<String> futureTaskB = POOL_EXECUTOR.submit(() -> {
            return doSomethingB();
        });
        String resultA = futureTask.get();
        String resultB = futureTaskB.get();
        System.out.println(resultA+" "+resultB);
        System.out.println(System.currentTimeMillis()-start);
        POOL_EXECUTOR.shutdownNow();

    }

    private static void testDemo() throws InterruptedException, ExecutionException {
        FutureTask<String> futureTask = new FutureTask<>(() -> {
            String result = doSomethingA();
            return result;
        });
        Thread thread = new Thread(futureTask, "threadA");
        thread.start();
        String taskBResult = doSomethingB();
        String taskAResult = futureTask.get();
        System.out.println(taskAResult+" "+taskBResult);
    }
}
