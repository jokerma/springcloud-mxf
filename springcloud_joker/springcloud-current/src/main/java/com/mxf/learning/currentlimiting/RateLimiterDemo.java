package com.mxf.learning.currentlimiting;

import com.google.common.util.concurrent.RateLimiter;

public class RateLimiterDemo {

    public static void main(String[] args) {
        // 创建一个每秒允许2个令牌（即两次操作）通过的RateLimiter实例
        RateLimiter rateLimiter = RateLimiter.create(2.0);

        for (int i = 1; i <= 10; i++) {
            // 请求获取令牌，如果没有可用令牌，则阻塞等待
            if (rateLimiter.tryAcquire()) {

                System.out.println("Task " + i + " acquired");
                // 执行任务逻辑（此处为了演示效果，只打印输出）
                doTask(i);
            } else {
                System.out.println("Task " + i + " reject");
            }
        }
    }

    private static void doTask(int taskId) {
        System.out.println("Executing task: " + taskId);
    }
}
