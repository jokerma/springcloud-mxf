package com.mxf.learning.threads;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * ThreadTest
 *
 * @author maxiaofeng
 * @date 2024/4/29
 */

class MyThread extends Thread{
    @Override
    public void run() {
        System.out.println(this.getState());
        System.out.println("I am a child thread");
    }
}

class RunableTask implements Runnable{

    @Override
    public void run() {

        System.out.println(Thread.currentThread().getName()+" :I am a child thread");
    }
}

class CallerTask implements Callable<String>{

    @Override
    public String call() throws Exception {
        return "hello jokerma";
    }
}

public class ThreadTest {


    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //testThreadExtend();
        //testRunnable();
        CallerTask callerTask = new CallerTask();
        FutureTask<String> futureTask = new FutureTask<>(callerTask);
        Thread thread = new Thread(futureTask);
        thread.start();
        System.out.println(futureTask.get());
    }

    private static void testRunnable() {
        RunableTask runableTask = new RunableTask();
        Thread thread = new Thread(runableTask);
        Thread thread2 = new Thread(runableTask);
        thread.start();
        thread2.start();
    }

    private static void testThreadExtend() {
        MyThread myThread = new MyThread();
        System.out.println(myThread.getState());
        myThread.start();
        System.out.println(myThread.getState());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(myThread.getState());
    }
}

