package com.mxf.learning.lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * SampleDemo
 *
 * @author maxiaofeng
 * @date 2024/4/29
 */
public class SampleDemo {
    public static String rpcCall(String ip, String param) {
        System.out.println(ip + " rpcCall:" + param);
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return param+":rt";
    }

    public static void main(String[] args){
        //sequenceTest();
        List<String> ipList = new ArrayList<>();
        for (int i = 0; i <= 10; i++) {
            ipList.add("192.168.0."+i);
        }
        // 并发调用
        long start = System.currentTimeMillis();
        List<CompletableFuture<String>> futureList =
                ipList.stream().map(ip -> CompletableFuture.supplyAsync(() -> rpcCall(ip, ip))).toList();
        // 3.等待所有异步任务执行完毕
        List<String> results = futureList.stream().map(future -> future.join()).toList();

        // 输出
        results.stream().forEach(System.out::println);
        System.out.println("cost: "+(System.currentTimeMillis()-start));



    }

    private static void sequenceTest() {
        List<String> ipList = new ArrayList<>();
        for (int i = 0; i <= 10; i++) {
            ipList.add("192.168.0."+i);
        }
        long start = System.currentTimeMillis();
        List<String> result= new ArrayList<>();
        for (String ip : ipList) {
            result.add(rpcCall(ip,ip));
        }
        result.stream().forEach(r-> System.out.println(r));
        System.out.println("cost:"+(System.currentTimeMillis()-start));
    }

}
