package com.mxf.learning.chapter01;

/**
 * ThreadCommonMethod
 *
 *
 * @author maxiaofeng
 * @date 2024/4/26
 */
public class ThreadCommonMethod {
    public static void main(String[] args) throws InterruptedException {
        //getCurrentThread();
        //setCurrentThreadName();
        //setPriority();
        //yeildThread();
        //sleepThread();
        //excuteJoin();
        Thread te = new Thread(() -> {
            System.out.println("start");
            for (int i = 0; i < 100; i++) {
                System.out.println("te: " + i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        te.setDaemon(true);
        te.start();
        Thread.sleep(100);
        System.out.println("end");

    }

    private static void excuteJoin() throws InterruptedException {
        Thread te = new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("te: " + i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        te.start();
        for (int i = 0; i < 100; i++) {
            System.out.println("main: "+i);
            Thread.sleep(1000);
            if(i==1){
                //te.join();
                te.join(2000);
            }
        }
    }

    private static void sleepThread() throws InterruptedException {
        System.out.println(System.currentTimeMillis());
        Thread.sleep(1000);
        System.out.println(System.currentTimeMillis());
    }

    private static void yeildThread() {
        Thread te = new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                if (i == 50) {
                    Thread.yield();
                    System.out.println("=====");
                }
                System.out.println("te: " + i);
            }
        });

        Thread tf = new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("tf: " + i);
            }
        });
        te.start();
        tf.start();
    }

    private static void setPriority() {
        Thread thread = new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("t: " + i);
            }
        });
        Thread threadClone = new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("tC: " + i);
            }
        });
        thread.setPriority(10);
        threadClone.setPriority(1);
        threadClone.start();
        thread.start();
    }

    private static void setCurrentThreadName() {
        Thread t1 = new Thread(() -> {
            System.out.println(Thread.currentThread().getName());
        });
        t1.setName("模块-功能-计算器");
        System.out.println(t1);
        t1.start();
    }

    private static void getCurrentThread() {
        // 获取当前线程
        Thread main = Thread.currentThread();
        System.out.println(main);
    }
}
