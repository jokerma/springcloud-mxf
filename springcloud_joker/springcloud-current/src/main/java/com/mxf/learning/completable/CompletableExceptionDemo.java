package com.mxf.learning.completable;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * CompletableExceptionDeom
 *
 * @author maxiaofeng
 * @date 2024/4/29
 */
public class CompletableExceptionDemo{
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CompletableFuture<String> future = new CompletableFuture<String>();

        new Thread(()->{
            try {
                if(true){
                    throw new RuntimeException("excetion test");
                }
                // 设置正常结果
                future.complete("ok");
            } catch (RuntimeException e) {
                // 设置异常结果
                future.completeExceptionally(e);
            }
            System.out.println("-----"+Thread.currentThread().getName()+" set future result -----");
        },"thread-1").start();
        // 输出异常信息
        //System.out.println(future.get());
        // 有异常输出默认值
        System.out.println(future.exceptionally(t->"default").get());



    }
}
