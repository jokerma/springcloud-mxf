package com.mxf.learning.currentlimiting;

import io.github.bucket4j.*;

import java.time.Duration;

public class Bucket4JDemo {

    public static void main(String[] args) throws InterruptedException {
        // 创建每秒允许2个令牌通过（即两次操作）的RateLimiter实例
        Bucket rateLimiter = createRateLimiter(2);

        for (int i = 1; i <= 10; i++) {
            // 请求获取令牌，如果没有可用令牌，则阻塞等待
            rateLimiter.asScheduler().consume(1);
            System.out.println("Task " + i + " acquired at nanosecond: ");

            // 执行任务逻辑（此处为了演示效果，只打印输出）
            doTask(i);
        }
    }

    private static Bucket createRateLimiter(int permitsPerSecond) {
        // 定义令牌桶配置
        Bandwidth limit = Bandwidth.simple(permitsPerSecond, Duration.ofSeconds(1));

        // 创建基于内存的Bucket实例
        return Bucket4j.builder().addLimit(limit).build();
    }
    private static void doTask(int taskId) {
      System.out.println("Executing task: " + taskId); 
       System.out.println("Executing task: " + taskId); 
     }
}
