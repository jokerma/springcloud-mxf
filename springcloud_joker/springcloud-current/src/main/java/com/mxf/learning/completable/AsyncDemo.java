package com.mxf.learning.completable;

import java.util.concurrent.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * AsyncDemo
 *
 * @author maxiaofeng
 * @date 2024/4/28
 */
public class AsyncDemo {

    private static final ThreadPoolExecutor bizPoolExecutor = new ThreadPoolExecutor(
            8,8,1, TimeUnit.MINUTES,new LinkedBlockingDeque<>(10)
    );
    static{

    }
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // runAsyncTest(); // 获取结果阻塞进程
        // runAsyncTestPool();
        // supplyAsyncTest(); //可以返回任务执行结果
        // supplyAsyncPoolTest();
        // thenRunDemo(); //B执行拿不到A的结果
        // thenRunAsyncDemo();
        // thenAcceptDemo(); //B执行可以拿到A的结果
        // thenAcceptAsyncDemo();
        // thenApplyDemo(); //B执行可以拿到A的结果,并且可以返回结果
        //thenApplyAsyncDemo();
        whenComplateDemo(); //異步非阻塞
    }

    private static void whenComplateDemo() throws InterruptedException {
        CompletableFuture<String> oneFuture = CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
                return "one Return";
            }
        });
        oneFuture.whenComplete(new BiConsumer<String, Throwable>() {
            @Override
            public void accept(String s, Throwable throwable) {
                System.out.println(Thread.currentThread().getName());
                if(null==throwable){
                    System.out.println(s);
                }else{
                    System.out.println(throwable.getMessage());
                }
            }
        });
        System.out.println("--- 无阻塞 ---");
        Thread.currentThread().join();
    }

    private static void thenApplyAsyncDemo() throws InterruptedException, ExecutionException {
        CompletableFuture<String> oneFuture = CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
                return "one Return";
            }
        });
        CompletableFuture<String> twoFuture = oneFuture.thenApplyAsync(new Function<String, String>() {
            @Override
            public String apply(String s) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
                return s + "-->apply two";
            }
        },bizPoolExecutor);
        System.out.println(twoFuture.get());
    }

    private static void thenApplyDemo() throws InterruptedException, ExecutionException {
        CompletableFuture<String> oneFuture = CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
                return "one Return";
            }
        });
        CompletableFuture<String> twoFuture = oneFuture.thenApply(new Function<String, String>() {
            @Override
            public String apply(String s) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
                return s + "-->apply two";
            }
        });
        System.out.println(twoFuture.get());
    }

    private static void thenAcceptAsyncDemo() throws InterruptedException, ExecutionException {
        CompletableFuture<String> oneFuture = CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
                return "hello,jiaduo";
            }
        });
        CompletableFuture<Void> twoFuture = oneFuture.thenAcceptAsync(new Consumer<String>() {
            @Override
            public void accept(String s) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
                System.out.println("--- after oneFuture over doSomething ----" + s);
            }
        },bizPoolExecutor);
        System.out.println(twoFuture.get());
    }

    private static void thenAcceptDemo() throws InterruptedException, ExecutionException {
        CompletableFuture<String> oneFuture = CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
                return "hello,jiaduo";
            }
        });
        CompletableFuture<Void> twoFuture = oneFuture.thenAccept(new Consumer<String>() {
            @Override
            public void accept(String s) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
                System.out.println("--- after oneFuture over doSomething ----" + s);
            }
        });
        System.out.println(twoFuture.get());
    }

    private static void thenRunAsyncDemo() throws InterruptedException, ExecutionException {
        CompletableFuture<Object> oneFuture = CompletableFuture.supplyAsync(new Supplier<Object>() {
            @Override
            public String get() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
                return "hello,jiaduo";
            }
        });
        CompletableFuture<Void> twoFuture = oneFuture.thenRunAsync(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
                System.out.println("---after oneFuture over doSomething---");
            }
        },bizPoolExecutor);
        System.out.println(oneFuture.get());
        System.out.println(twoFuture.get());
    }

    private static void thenRunDemo() throws InterruptedException, ExecutionException {
        CompletableFuture<Object> oneFuture = CompletableFuture.supplyAsync(new Supplier<Object>() {
            @Override
            public String get() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
                System.out.println("---after oneFuture over doSomething---");
                return "hello,jiaduo";
            }
        });
        CompletableFuture<Void> twoFuture = oneFuture.thenRun(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
                System.out.println("---after twoFuture over doSomething---");
            }
        });
        System.out.println(twoFuture.get());
    }

    private static void supplyAsyncPoolTest() throws InterruptedException, ExecutionException {
        CompletableFuture<Object> future = CompletableFuture.supplyAsync(new Supplier<Object>() {
            @Override
            public String get() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
                return "hello,jiaduo";
            }
        }, bizPoolExecutor);
        System.out.println(future.get());
    }

    private static void supplyAsyncTest() throws InterruptedException, ExecutionException {
        CompletableFuture<String> future = CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName());
                return "hello,jiaduo";
            }
        });
        System.out.println(future.get());
    }

    private static void runAsyncTestPool() throws InterruptedException, ExecutionException {
        CompletableFuture<Void> future = CompletableFuture.runAsync(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + ": over");
            }
        },bizPoolExecutor);
        System.out.println("--- main run ---");
        System.out.println(future.get());
    }

    private static void runAsyncTest() throws InterruptedException, ExecutionException {
        long start = System.currentTimeMillis();
        CompletableFuture future = CompletableFuture.runAsync(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("over");
            }
        });
        System.out.println(System.currentTimeMillis()-start);
        System.out.println(future.get());
    }
}
