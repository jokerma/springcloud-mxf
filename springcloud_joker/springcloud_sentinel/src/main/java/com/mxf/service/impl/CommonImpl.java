package com.mxf.service.impl;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.mxf.service.Common;
import org.springframework.stereotype.Service;

@Service
public class CommonImpl implements Common {
    @Override
    @SentinelResource(value = "common")
    public String getCommon() {
        return "common";
    }
}
