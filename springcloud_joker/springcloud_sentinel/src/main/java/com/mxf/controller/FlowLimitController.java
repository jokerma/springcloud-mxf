package com.mxf.controller;

import com.mxf.service.Common;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
@Slf4j
@RequestMapping("/sentenial")
public class FlowLimitController {
    @Autowired
    private Common common;

    @GetMapping("testA")
    public String testA() {
//        try {
//            TimeUnit.MILLISECONDS.sleep(800);
//        } catch (InterruptedException e) {
//            throw new RuntimeException(e);
//        }
        log.info(Thread.currentThread().getName()+"：testA");
        String commons = common.getCommon();
        return commons + "-------------testA";
    }

    @GetMapping("testB")
    public String testB() {
        String commons = common.getCommon();
        return commons + "-------------testB";
    }


    @GetMapping("testC")
    public String testC() {
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        String commons = common.getCommon();
        return commons + "-------------testC";
    }

    /**
     * 异常比例
     * @param id
     * @return
     */
    @GetMapping("testD")
    public String testD(Integer id) {
        if (id!=null && id>1) {
            throw new RuntimeException("异常比例调试");
        }
        String commons = common.getCommon();
        return commons + "-------------testD";
    }

    /**
     * 异常数
     * @param id
     * @return
     */
    @GetMapping("testE")
    public String testE(Integer id) {
        if (id!=null && id>1) {
            throw new RuntimeException("异常数据调试");
        }
        String commons = common.getCommon();
        return commons + "-------------testE";
    }
}
