package com.mxf.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/consumerB")
@Slf4j
public class ConsumerFilterController {
    @Value("${server.port}")
    private String serverPort;
    @Value("${spring.application.name}")
    private String serverName;
    @GetMapping("/serverPortB")
    public String getServerPort(String param){
        log.info("======== get serverPort confilter {}",param);
        return serverName+"-"+serverPort;
    }

    @GetMapping(value = "/customB")
    public String customTest(){
        return "网关配置测试~~confilter";
    }
}
