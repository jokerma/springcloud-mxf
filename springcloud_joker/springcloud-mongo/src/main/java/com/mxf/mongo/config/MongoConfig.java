package com.mxf.mongo.config;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.SpringDataMongoDB;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;

import java.util.concurrent.TimeUnit;
/**
 * MongoConfig
 *
 * @author maxiaofeng
 * @date 2024/4/26
 */

@Configuration
public class MongoConfig {
    @Bean
    @Primary
    public MongoProperties operationMongoProperties() {
        return new MongoProperties();
    }
    /**
     * 运营中心连接池自定义配置处理
     *
     * @return
     */
    @Bean
    public MongoDatabaseFactory mongoDatabaseFactory(MongoProperties properties) {
        MongoClientSettings.Builder builder = MongoClientSettings.builder();
        builder.applyConnectionString(new ConnectionString(properties.getUri()));
        builder.applyToConnectionPoolSettings(b -> {
            // 允许的最大连接数
            b.maxSize(100);
            // 最小连接数
            b.minSize(5);
            // 池连接可以存活的最长时间。
            b.maxConnectionLifeTime(0, TimeUnit.SECONDS);
            // 池连接的最大空闲时间。
            b.maxConnectionIdleTime(5, TimeUnit.MINUTES);
            // 默认最大连接时间120s;
            b.maxWaitTime(60000, TimeUnit.MILLISECONDS);
        });
        MongoClient mongoClient = MongoClients.create(builder.build(), SpringDataMongoDB.driverInformation());
        return new SimpleMongoClientDatabaseFactory(mongoClient, properties.getDatabase());
    }
    @Bean
    public MongoTemplate mongoTemplate(MongoDatabaseFactory mongoDatabaseFactory) {
        MongoTemplate mongoTemplate = new MongoTemplate(mongoDatabaseFactory);
        MappingMongoConverter mongoMapping = (MappingMongoConverter) mongoTemplate.getConverter();
        mongoMapping.setTypeMapper(new DefaultMongoTypeMapper(null));
        mongoMapping.afterPropertiesSet();
        return mongoTemplate;
    }
}
