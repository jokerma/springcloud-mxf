package com.mxf.config;

import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.EntryType;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
 
import java.util.ArrayList;
import java.util.List;
 
public class UserRateLimitDemo {
    static {
        initFlowRules();
    }
 
    public static void initFlowRules() {
        List<FlowRule> rules = new ArrayList<>();
        FlowRule rule = new FlowRule();
        rule.setResource("user-resource");
        rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
        // 每秒允许单个用户访问1次
        rule.setCount(1);
        // 设置用户的标识为 "user-id"
        rule.setLimitApp("user-id");
        rules.add(rule);
 
        FlowRuleManager.loadRules(rules);
    }
 
    public static void main(String[] args) {
        while (true) {
            Entry entry = null;
            try {
                // 添加注解识别当前接口是否需要精心用户限制
                // 用户标识通常是通过用户登录后获取的，这里简单用一个固定值代替
                entry = SphU.entry("user-resource", EntryType.IN, 1, "user-id");
                // 被保护的业务逻辑
                System.out.println("Business handling");
            } catch (BlockException e) {
                // 处理被限流后的业务逻辑
                System.out.println("Blocked!");
            } finally {
                if (entry != null) {
                    entry.exit();
                }
            }
        }
    }
}